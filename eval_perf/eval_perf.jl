include("../conf_cplex.jl")
include("../plne1_v1.jl")
include("../plne2_v1.jl")
include("../dantzig_wolfe.jl")
using Plots
using Dates
using StatsPlots
using DataFrames

#Liste des méthodes implémentées, à compléter au fur et à mesure du projet
#/!\ Penser à bien inclure les fichiers avec les fonction de résolution
Methodes = ["plne1","plne2","dw"]

#These two functions are here to convert the output to the right format.
function plne1_format(model, Y, Z, t)
  return JuMP.primal_status(model) == JuMP.MathOptInterface.FEASIBLE_POINT, t, round(objective_value(model)),MOI.get(model,MOI.RelativeGap()),JuMP.value.(Y), JuMP.value.(Z)
end

function plne2_format(model, Y, Y_bar, Z, t)
  return(JuMP.primal_status(model) == JuMP.MathOptInterface.FEASIBLE_POINT, t, round(objective_value(model)),MOI.get(model,MOI.RelativeGap()),JuMP.value.(Y),JuMP.value.(Y_bar), JuMP.value.(Z))
end

"""
Génération aléatoire et écriture de fichiers d'instances avec :
 - un nombre de noeuds variant de n_min à n_max (avec un pas de 1)
 - une probabilité que chaque couple de noeuds soit relié par une arête variant de 0 à 1 (avec un pas step_p)

Arguments
	- n_min: nombre minimal de noeuds dans les instances
	- n_max: nombre maximal de noeuds dans les instances
	- step_p: pas avec lequel varie la probabilité que chaque couple de noeuds soit relié par une arête
	- dataFolder: nom du dossier où les fichiers d'instances seront sauvegardés
	
Exemple: generateData(2, 20, 0.2, "./dataA/")
		 generateData(2, 10, 0.05, "./dataB/") #génère des instances de n = 2 à n = 10 avec proba de 0 à 1 avec un pas de 0.05 
"""
function generateData(n_min::Int64, n_max::Int64, step_p::Float64, dataFolder::String)

	# Créer le dossier où seront sauvegardé les fichiers d'instances, si nécessaire
	if !isdir(dataFolder)
		mkdir(dataFolder)
	end
	
	# Construire et écrire une instance pour chaque valeur de n et p
	for n in n_min:n_max
		for p in collect(0:step_p:1)
			fileName = dataFolder * "instance" * "_n" * string(n) * "_p" * string(p)*"_"*(Dates.format(Dates.now(), "yyyy-mm-dd-HH-MM-SS")) # ex: instance_n13_p0.95_2021-02-28-20-45-40
			println("n=$n, p=$p, file=$fileName")
			# Générer et écrire en appelant la fonction generate(n::Int, p::Float64, filename::String) de instances_io.jl
			generate(n,p,fileName)
		end
	end
end


"""
Résolution d'une instance par une méthode donnée et écriture du résultat dans un fichier

Arguments
	- resolutionMethod: nom de la méthode de résolution parmi Methodes
	- fileName: nom de l'instance à résoudre
	- dataFolder: nom du dossier où se trouve l'instance à résoudre
	- resFolder: nom du dossier où le résultat sera sauvegardé
	
Exemple: writeRes("plne1", "instance_n7_p0.6", "./data/", "./res/")
"""
function writeRes(resolutionMethod::String, fileName::String, dataFolder::String, resFolder::String)
	
	# Tester si le fichier d'instance existe bien
	if !isfile(dataFolder*fileName)
		println("Erreur dans writeRes: fichier d'entrée inexistant: ", dataFolder*fileName)
		return
	end
	
	# Tester si la méthode choisie existe bien
	if !(resolutionMethod in Methodes)
		println("Erreur dans writeRes: méthode choisie inexistante ", resolutionMethod)
		println("Créer la méthode ou inclure le fichier avec la fonction de résoltion et ajouter la méthode à la liste des méthodes possibles")
		return
	end
	
	# Créer le dossier de sauvegarde des résultats, si nécessaire
	if !isdir(resFolder)
		mkdir(resFolder)
	end

	# Créer le sous-dossier de sauvegarde des résultats correspondand à la méthode de résolution, si nécessaire
	resMethodFolder = resFolder * resolutionMethod * "/"
	if !isdir(resMethodFolder)
		mkdir(resMethodFolder)
	end
	
	# Lire le fichier d'instance en appelant la fonction generate(filename::String) de instance_io.jl
	g = generate(dataFolder * fileName)
	
	println("="^10, " Résolution de $fileName avec la méthode $resolutionMethod")
	outputFile = resMethodFolder * fileName
	println("="^10, " Fichier de sauvegarde du résultat : $outputFile")
	fout = open(outputFile, "w")  
	resolutionTime = -1
	isOptimal = false

	if resolutionMethod == "plne1"
		# Résoudre l'instance en appelant la fonction plne1_solve(g::SimpleGraph) de plne1_v1.jl
		isFeasible, resolutionTime, value, gap, y_sol, z_sol = plne1_format(plne1_solve(g))
	elseif resolutionMethod == "plne2"
		# Résoudre l'instance en appelant la fonction plne2_solve(g::SimpleGraph) de plne2_v1.jl
		isFeasible, resolutionTime, value, gap, y_sol, y_bar_sol, z_sol = plne2_format(plne2_solve(g))
	end
	
	if isFeasible
		println(fout,"isFeasible = ", isFeasible)
		println(fout,"obj = ", value)
		println(fout,"gap = ", gap)
		println(fout,"solveTime = ", resolutionTime)
		
		#noeuds et arêtes
		nb_vertices = nv(g)
		nb_cliques = 2*nb_vertices
		vertices = 1:nb_vertices
		edges = []
		edges_bar = []
		for s in vertices 
			for d in (s+1):nb_vertices
				if has_edge(g, s, d)
					push!(edges, (s,d))
				else 
					push!(edges_bar, (s,d))
				end
			end
		end
  
		println(fout,"n = ",nb_vertices)
		println(fout,"p = ",parse(Float64,fileName[end-22:end-20]))
		println(fout,"edges = ",edges)
		println(fout,"edges_bar = ",edges_bar)
		
		#variable y[edge,clique]
		println(fout,"y_sol = [")
		for e in edges
			print(fout,"[",round(Int64,y_sol[e,1]))
			for c in 2:nb_cliques
				print(fout,",",round(Int64,y_sol[e,c]))
			end
			println(fout,"]")
		end
		println(fout,"];")			

		if resolutionMethod == "plne2"
			#variable y_bar[edge_bar,clique]
			println(fout,"y_bar_sol = [")
			for eb in edges_bar
				print(fout,"[",round(Int64,y_bar_sol[eb,1]))
				for c in 2:nb_cliques
					print(fout,",",round(Int64,y_bar_sol[eb,c]))
				end
				println(fout,"]")
			end
			println(fout,"];")
		end
		
		#variable z[vertice,clique]
		println(fout,"z_sol = [")
		for v in 1:nb_vertices
			print(fout,"[",round(Int64,z_sol[v,1]))
			for c in 2:nb_cliques
				print(fout,",",round(Int64,z_sol[v,c]))
			end
			println(fout,"]")
		end
		println(fout,"];")
  
	else 
		println(fout,"isFeasible = ", isFeasible)
		println(fout,"solveTime = ",resolutionTime)
	end
	close(fout)
end


"""
Résolution de toutes les instances présentes dans d'un dossier par une méthode donnée et écriture des résultats dans des fichiers

Arguments
	- resolutionMethod: nom de la méthode de résolution parmi Methodes
	- dataFolder: nom du dossier où se trouve toutes les instances à résoudre
	- resFolder: nom du dossier où les résultats seront sauvegardés
	
Exemple: writeResAll("plne1", "./data/", "./res/")
		 writeResAll("plne2", "./data/", "./res/")
"""
function writeResAll(resolutionMethod::String,dataFolder::String,resFolder::String)
	# Résoudre chaque instance et écrire le résultat en appelant la fonction writeRes(resolutionMethod::String, fileName::String, dataFolder::String, resFolder::String)
	for file in readdir(dataFolder)
		writeRes(resolutionMethod, file, dataFolder, resFolder)
	end
end


"""
Tracé du temps de calcul moyen en fonction du nombre de noeuds dans les instances résolues lues
Sauvegarde de la courbe dans un fichier pdf

Arguments
	- n_max: nombre maximal de noeuds à considérer
	- resFolder: nom du dossier où se trouvent les instances résolues
	- outputFile: nom du fichier avec la courbe tracée
	
Exemple: showPerfNodes(10,"./resA/plne1/","courbe_perf_plne1_noeuds.pdf") 
"""
function showPerfNodes(n_max::Int64,resFolder::String,outputFile::String)

	# Tableau où time[i] = somme des temps de calcul des instances qui ont i noeuds 
	time = zeros(n_max)
	
	# Tableau où countn[i] = nb d'instances qui ont i noeuds 
	countn = zeros(n_max)
	
	# Dataframe avec autant de ligne que d'instances, une colonne avec le nombre de noeuds de l'instance, une colonne pour le temps de résolution
	df = DataFrame(nb_vertices=Int64[],time_res=Float64[])
	
	maxSolveTime = 0
	
	# Parcourir les fichiers, lire le nombre de noeuds et le temps de calcul
	for resultFile in readdir(resFolder)
		path = resFolder * resultFile
		println(path)
		include(path)
		if isFeasible && n<=n_max
			time[n] = time[n]+solveTime
			countn[n]+=1
			push!(df,[n solveTime])
			if solveTime > maxSolveTime
				maxSolveTime = solveTime
			end 
		end 
	end

	println("Max solve time: ", maxSolveTime)
	
	# Tracer l'évolution du temps de calcul MOYEN en fonction du nombre de noeuds
	x = [1:n_max]
	y = time./countn
	fig = plot(x, y, xaxis = "Nombre de noeuds", yaxis = "Temps de résolution moyen (s)",label="",title=resFolder)
	fig = scatter!(x,y,color="blue",label="")
	savefig(fig,outputFile)

	# Tracer l'évolution du temps de calcul en fonction du nombre de noeuds avec des BOITES A MOUSTACHES
	println(df)
	df_plot = @df df boxplot(:nb_vertices,:time_res,xaxis = "Nombre de noeuds", yaxis = "Temps de résolution (s)",label="",fill=(:white),line=(:black),marker=(0.5,:black),title=resFolder)
	savefig(df_plot,outputFile[1:end-4]*"_boxplot.pdf")
	
	return x, y
end 


"""
Tracé du temps de calcul moyen en fonction de la probabilité de création d'arêtes dans les instances résolues lues, pour un nombre de noeuds fixé
Sauvegarde de la courbe dans un fichier pdf

Arguments
	- nb_nodes : nombre de noeuds à considérer
	- resFolder: nom du dossier où se trouvent les instances résolues
	- outputFile: nom du fichier avec la courbe tracée
	
Exemple: showPerfProba(5,"./resB/plne1/","courbe_perf_plne1_proba.pdf") 
"""
 function showPerfProba(nb_nodes::Int64,resFolder::String,outputFile::String)

	# Dataframe avec autant de lignes que d'instances, une colonne avec la proba de création d'arêtes, une colonne pour le temps de résolution
	df = DataFrame(proba=Float64[],time_res=Float64[])
	
	maxSolveTime = 0
	
	# Parcourir les fichiers, lire la proba et le temps de calcul
	for resultFile in readdir(resFolder)
		p = parse(Float64,resultFile[end-22:end-20]) # nécessaire pour les instances du dossier ./resB/plne1 où p n'a pas été écrit (il faudrait réécrire les fichiers...)
		path = resFolder * resultFile
		println(path)
		include(path)
		println(p)
		if isFeasible && n == nb_nodes
			push!(df,[p solveTime])
			if solveTime > maxSolveTime
				maxSolveTime = solveTime
			end 
		end 
	end

	println("Max solve time: ", maxSolveTime)
	println(df)
	
	# Tracer l'évolution du temps de calcul MOYEN en fonction de la probabilité
	df_mean = combine(groupby(df,:proba),df -> mean(df[!,:time_res]))
	rename!(df_mean,["proba","time_res_mean"])
	println(df_mean)
	df_mean_plot = @df df_mean scatter(:proba,:time_res_mean,xaxis = "Probabilité de création d'arêtes", yaxis = "Temps de résolution moyen (s)",color="blue",label="",title=resFolder*" $nb_nodes noeuds")
	savefig(df_mean_plot,outputFile[1:end-4]*"_n$nb_nodes.pdf")

	# Tracer l'évolution du temps de calcul en fonction de la proba de création d'arêtes avec des BOITES A MOUSTACHES
	# df_boxplot = @df df boxplot(:proba,:time_res,xaxis = "Probabilité de création d'arêtes", yaxis = "Temps de résolution (s)",label="",fill=(:white),line=(:black),marker=(0.5,:black),title=resFolder*" $nb_nodes noeuds")
	# savefig(df_boxplot,outputFile[1:end-4]*"_n$nb_nodes"*"_boxplot.pdf")
	
	return df_mean
 end 


"""
1. Génération aléatoire et écriture de fichiers d'instances: "iter" fichiers auront le même nombre de noeuds et même proba. Cela permettra de faire des moyennes
2. Résolution de toutes ces instances par les méthodes dans Methodes et écriture des résultats dans des fichiers

Arguments
	- iter: nombre de jeu de données à générer
	- n_min: nombre minimal de noeuds dans les instances générées
	- n_max: nombre minimal de noeuds dans les instances générées
	- step_p: pas avec lequel varie la probabilité que chaque couple de noeuds soit relié par une arête dans les instances générées
	- dataFolder: nom du dossier où les fichiers d'instances générées seront sauvegardés
	- resFolder: nom du dossier nom du dossier où les résultats seront sauvegardés

Exemple: doAll(3, 2, 20, 0.2, "./dataA/", "./resA/")
		 doAll(15, 5, 5, 0.05, "./dataB/", "./resB/")
"""
function doAll(iter::Int64, n_min::Int64, n_max::Int64, step_p::Float64, dataFolder::String, resFolder::String)
	
	#1. Créer plusieurs jeux de données identiques (même nombre de noeud et même proba)
	for i in 1:iter
		generateData(n_min, n_max, step_p, dataFolder)
		sleep(1.0) # pause de 1s pour que les fichiers générés à l'itération suivante n'aient pas le même nom
	end

	#2.Résoudre toutes ces instances par toutes les méthodes
	for resolutionMethod in Methodes
		writeResAll(resolutionMethod,dataFolder,resFolder)
	end

end

"""
# 3. Tracé du temps de calcul en fonction du nombre de noeuds dans les instances, sauvegarde de la courbe dans un fichier pdf
# 4. Tracé du temps de calcul en fonction de la proba de création d'arêtes dans les instances, sauvegarde de la courbe dans un fichier pdf

Fonction non générique, utilisée pour tracer les courbes du rapport
"""
function plotAll()

	#3. Tracer le temps de calcul en fonction du nombre de noeuds
	x1,y1 = showPerfNodes(20,"./resA/plne1/","courbe_perf_plne1_noeuds.pdf") 
	x2,y2 = showPerfNodes(20,"./resA/plne2/","courbe_perf_plne2_noeuds.pdf") 
	fig = plot(x1, y1, xaxis = "Nombre de noeuds", yaxis = "Temps de résolution moyen (s)",label="PLNE1",legend=:topleft,title="Comparaison des méthodes")
	fig = scatter!(x1,y1,color="blue",label="")
	fig = plot!(x1, y2, color="red",label="PLNE2",legend=:topleft)
	fig = scatter!(x1,y2,color="red",label="")
	savefig(fig,"courbe_perf_noeuds.pdf")
	
	#Zoom sur les faibles valeurs de nb de noeuds
	x1,y1 = showPerfNodes(9,"./resA/plne1/","courbe_perf_plne1_noeuds_zoom.pdf") 
	x2,y2 = showPerfNodes(9,"./resA/plne2/","courbe_perf_plne2_noeuds_zoom.pdf") 
	fig = plot(x1, y1, xaxis = "Nombre de noeuds", yaxis = "Temps de résolution moyen (s)",label="PLNE1",legend=:topleft,title="Comparaison des méthodes")
	fig = scatter!(x1,y1,color="blue",label="")
	fig = plot!(x1, y2, color="red",label="PLNE2",legend=:topleft)
	fig = scatter!(x1,y2,color="red",label="")
	savefig(fig,"courbe_perf_noeuds_zoom.pdf")
	
	# 4. Tracer le temps de calcul en fonction de la probabilité de création d'arêtes pour un nombre de noeuds i fixé
	for i in 2:10
		df_mean1 = showPerfProba(i,"./resB/plne1/","courbe_perf_plne1_proba.pdf")
		df_mean2 = showPerfProba(i,"./resB/plne2/","courbe_perf_plne2_proba.pdf") 
		df_mean_plot = @df df_mean1 scatter(:proba,:time_res_mean,xaxis = "Probabilité de création d'arêtes", yaxis = "Temps de résolution moyen (s)",color="blue",label="PLNE1",legend=:topleft,title="Comparaison avec $i noeuds")
		df_mean_plot = @df df_mean2 scatter!(:proba,:time_res_mean,color="red",label="PLNE2",legend=:topleft)
		savefig(df_mean_plot,"courbe_perf_proba_n$i.pdf")
	end
end
