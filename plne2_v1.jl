#include("./conf_cplex.jl")
if !@isdefined optimizer
  include("./conf_glpk.jl")
end

"""
Find a line-graph from a graph using 'PLNE2'

Arguments
- g : graph
- p : coefficient for objective function (alpha in the report)
- v : turn on/off the verbose

Return
- model, Y, Y_bar, Z : model & variables after solve
- t : time of the solve
"""
function plne2_solve(g::SimpleGraph, p::Float64 = 1., v::Bool = false)
  n = nv(g)
  m = ne(g)
  vertices = 1:n

  edges = []
  edges_bar = []
  for s in vertices 
    for d in (s+1):n
      if has_edge(g, s, d)
        push!(edges, (s,d))
      else 
        push!(edges_bar, (s,d))
      end
    end
  end

  println(edges)
  println(edges_bar)

  model = Model(optimizer)
  if @isdefined setup_model
    setup_model(model)
  end

  if v
    set_verbose(model)
  end

  @variable(model, Y[edges, 1:(2*n)], Bin)
  @variable(model, Y_bar[edges_bar, 1:(2*n)], Bin)
  @variable(model, Z[vertices, 1:(2*n)], Bin)

# Objective function

  @objective(model, Max, p*sum(Y[e, c] for c in 1:(2*n) for e in edges)
      - sum(Y_bar[e, c] for c in 1:(2*n) for e in edges_bar))

# Constraints
  for v in vertices 
    @constraint(model, sum(Z[v, c] for c in 1:(2*n)) >= 1)
    @constraint(model, sum(Z[v, c] for c in 1:(2*n)) <= 2)
  end

  for e in edges 
    @constraint(model, sum(Y[e, c] for c in 1:(2*n)) <= 1)
  end

  for e in edges_bar 
    @constraint(model, sum(Y_bar[e, c] for c in 1:(2*n)) <= 1)
  end

  for c in 1:(2*n)
    for s in vertices 
      for d in (s+1):n
        if !has_edge(g, s, d) 
          @constraint(model, Z[s, c] + Z[d, c] - 1 <= Y_bar[(s,d), c])
          @constraint(model, Y_bar[(s,d), c] <= Z[s, c])
          @constraint(model, Y_bar[(s,d), c] <= Z[d, c])
        else 
          @constraint(model, Z[s, c] + Z[d, c] - 1 <= Y[(s,d), c])
          @constraint(model, Y[(s,d), c] <= Z[s, c])
          @constraint(model, Y[(s,d), c] <= Z[d, c])
        end 
      end
    end
  end

  t = time()
  optimize!(model)
  t = time() - t

  return(model, Y, Y_bar, Z, t)
end


"""
Run tests for the 'PLNE2' method

Argument
- v : turn on/off the plots

Print
- objective value and resolution time of model
- if v : print values of variables Y,Z
"""
function plne2_run_unit_tests(v::Bool = false)
  path = "unit_tests/TU_0"
  for i in 1:9
    println(i)
    p = string(path, i)
    println(p)

    g = generate(p)

    model, Y, Z, t = plne2_solve(g)
    println(objective_value(model))
    println(t)
    if v
      println(value.(Y))
      println(value.(Z))
    end
  end
end

if abspath(PROGRAM_FILE) == @__FILE__
  plne2_run_unit_tests()
end
