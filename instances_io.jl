"""
Les fonctions ci-dessous lisent et génèrent des fichiers de test. Les fichiers de tests unitaires fournis avec ce fichier peuvent vous servir à tester vos algos (avec les tests unitaires) et à les évaluer (avec des grpahes que vous générez vous même). Les graphes générés sont des graphes simples. Il n'est pas possible d'avoir des boucles ou deux arêtes parallèles.

Le type utilisé pour manipuler des graphes en Julia est celui de la bibliothèque LightGraphs. 
Pour l'installer, il faut utiliser le gestionnaire de paquet de Julia : 

> julia
using Pkg
Pkg.add("LightGraphs")

Il existe une documentation plus ou moins claire en ligne : https://juliagraphs.org/LightGraphs.jl/latest/


Quelques fonctions de bases pour manipuler un graphe :
- nv(g) renvoie le nombre de noeuds de g
- ne(g) renvoie le nombre d'arêtes de g

Les noeuds sont représentés par des entiers entre 1 et nv(g).
Les fonctions suivantes permettent de modifier ou de lire le graphe et renvoient vrai ou faux selon que l'opération est un succès ou un échec (par exemple supprimer une arête qui n'existe pas).
- add_vertex!(g) rajoute un noeud au graphe
- rem_vertex!(g, v) retire le noeud v du graphe. Attention, cette opération supprime les arêtes incidentes à v ; de plus tous les noeuds entre v + 1 et nv(g) voient leur indice décalé de 1.
- add_edge!(g, u, v) rajoute une arête entre u et v
- rem_edge!(g, e) ou rem_edge!(g, u, v) retire l'arête e / entre u et v.
- has_edge!(g, e) ou has_edge!(g, u, v) vérifie s'il existe une arête e / entre u et v
- neighbors(g, u) renvoie les noeuds voisins de u dans le graphe
- vertices(g) renvoie un itérateur sur les noeuds de g (renvoie l'itérateur 1:nv(g))
- edges(g) renvoie un itérateur sur les arêtes de g
- src(e) et dst(e) renvoient les deux extrémités de l'arête e
"""

using LightGraphs
using StatsBase

@doc """
generate(n::Int, p::Float64)
- n ∈ N*
- p ∈ [0, 1]

Fonction de génération d'instance.

Cette fonction génère et renvoie une instance de SimpleGraph (de la bibliothèque LightGraphs) avec n noeuds. Chaque couple de noeuds est ensuite relié par une arête avec une probabilité p. 
""" ->
function generate(n::Int, p::Float64)
    g = SimpleGraph()
    add_vertices!(g, n)
    for u in 1:n
        for v in (u+1):n
            if rand() < p
                add_edge!(g, u, v)
            end
        end
    end
    return g
end


@doc """
linegraph(g::SimpleGraph)

Calcule et renvoie le linegraph de g. Le type du graphe renvoyé est SimpleGraph.
NDLA : Apparemment elle existe dans la bibliothèque, mais pas de mention dans la doc et impossible de la trouver dans Julia directement. Peut être pas encore sur la branche principale.
""" ->
function linegraph(g::SimpleGraph)
    h = SimpleGraph()
    es = [e for e in edges(g)]
    add_vertices!(h, ne(g))
    for (uh, e) in enumerate(es)
        for (vh, f) in enumerate(es)
           if uh >= vh
               continue
           end
           if (src(e) == src(f) || src(e) == dst(f) || dst(e) == src(f) || dst(e) == dst(f))
               add_edge!(h, uh, vh)
           end
        end
    end
    return h
end


@doc """
generate(n::Int, p::Float64, k::Int, filename::String)
- n ∈ N*
- p ∈ [0, 1]
- k ∈ N*

Génère un graphe avec la fonction generate(n, p).
Calcule ensuite son linegraphe et inverse k arêtes/non-arêtes de ce graphe aléatoirement. Renvoie ensuite le graphe obtenu.
""" ->
function generate(n::Int, p::Float64, k::Int)
    g = generate(n, p)
    h = linegraph(g)
    couples = [(u, v) for u in 1:n for v in (u + 1):n]
    if k < length(couples)
        couples = sample(couples, k, replace=false)
    end

    for (u, v) in couples
        if has_edge(h, u, v)
            rem_edge!(h, u, v)
        else
            add_edge!(h, u, v)
        end
    end
    return h
end

@doc """
to_file(g::SimpleGraph, filename::String)

Crée ou écrase le fichier filename et le rempli avec des informations relatives au graphe g.

Le format du fichier est le suivant:
- Une première ligne contenant un commentaire, commençant par #
- une ligne contenant le nombre n de noeuds
- n représentant la matrice d'adjacence : chaque ligne contient n bit 0 ou 1; le v-ieme nombre de la u-ieme ligne vaut 1 si et seulement s'il y a une arête entre u et v dans le graphe
""" ->
function to_file(g::SimpleGraph, filename::String)
    n = nv(g)
    open(filename, "w") do file
        write(file, "# Instance générée aléatoirement avec instance_io.jl\n")
        write(file, "$(n)\n")
        for u in 1:n
            for v in 1:n
                if has_edge(g, u, v)
                    write(file, "1")
                else
                    write(file, "0")
                end
            end
            write(file,"\n")
        end
    end
end

@doc """
generate(n::Int, p::Float64, filename::String)
- n ∈ N*
- p ∈ [0, 1]

Utilise la fonction `generate(n, p)` pour construire une instance puis écrit cette instance dans le fichier dont le chemin est `filename` avec la fonction to_file. Enfin, elle renvoie l'instance générée.

""" ->
function generate(n::Int, p::Float64, filename::String)
    g = generate(n, p)
    to_file(g, filename)
    return g
end


@doc """
generate(n::Int, p::Float64, k::Int, filename::String)
- n ∈ N*
- p ∈ [0, 1]
- k ∈ N*

Utilise la fonction `generate(n, p, k)` pour construire une instance puis écrit cette instance dans le fichier dont le chemin est `filename` avec la fonction to_file. Enfin, elle renvoie l'instance générée.

""" ->
function generate(n::Int, p::Float64, k::Int, filename::String)
    h = generate(n, p, k)
    to_file(h, filename)
    return h
end




@doc """ 
generate(filename::String)

Lecture fichier de données.

Cette fonction lit un fichier dont le chemin est `filename` et renvoie un objet de type SimpleGraph contenant les informations écrites dans le fichier.
Le fichier doit être au format généré par la fonction `generate(n::Int, p::Float64, filename::String)`
""" ->
function generate(filename::String)
  
  open(filename) do file
    lines = readlines(file)      
    g = SimpleGraph()
    n = parse(Int64, lines[2])

    add_vertices!(g, n)

    for (u, line) in enumerate(lines[3:n+2])
        for (v, c) in enumerate(line)
            if c == '1'
                add_edge!(g, u, v)
            end
        end
    end

    return g
  end
end
