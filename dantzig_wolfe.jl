include("./conf_cplex.jl")
if !@isdefined optimizer
  include("./conf_glpk.jl")
end


"""
Tell if an edge is part of a klick

Arguments
- e : edge of a graph
- klick : klick of a graph

Return
- boolean
"""
function edge_in_cg(e, klick)
  s, d = e
  flag = false
  for so in klick 
    if so == s 
      flag = true
    end
    if flag && (so == d)
      return true
    end
  end

  return false
end


"""
Tell if a vertice is part of a klick

Arguments
- v : vertice of a graph
- klick : klick of a graph

Return
- boolean
"""
function vert_in_cg(v, klick)
  for u in klick
    if u == v 
      return true
    end
  end
  return false
end


"""
model of the PRIMAL PROBLEM for Dantzig-Wolfe

Arguments
- vertices : set of all vertices
- edges : set of all edges
- klist : list of klicks

Return
- model, X : model & variables after solve
- t : time of the solve
"""
function primal(vertices, edges, klist)
  n = length(vertices)
  model = Model(optimizer)
  if @isdefined setup_model
    setup_model(model)
  end

  @variable(model, X[klist])

# Objective function
# The coeff attached to X[c] is the number of edges in c
  @objective(model, Max, 
      sum(X[c] * (length(c) * (length(c) - 1))/2 for c in klist))

# Constraints
  for v in vertices 
    @constraint(model, sum(X[c] for c in klist if vert_in_cg(v, c)) >= 1)
    @constraint(model, sum(X[c] for c in klist if vert_in_cg(v, c)) <= 2)
  end

  for e in edges 
    @constraint(model, sum(X[c] for c in klist if edge_in_cg(e, c)) <= 1)
  end

  for c in klist
    @constraint(model, X[c] >= 0)
  end

  @constraint(model, sum(X[c] for c in klist) <= 2*n)
  
  t = time()
  optimize!(model)
  t = time() - t

  return model, X, t 
end

"""
Model of the DUAL PROBLEM for Danzig-Wolfe
First thing to call
  -> give coefficients for the new sub-problem

Arguments
- vertices : set of all vertices
- edges : set of all edges
- klist : list of klicks

Return
- model, Mu1, Mu2, Omega, Eta : model & variables after solve
- t : time of the solve
"""
function dual(vertices, edges, klist)
  n = length(vertices)
  model = Model(optimizer)
  if @isdefined setup_model
    setup_model(model)
  end

# Variables
  @variable(model, Mu1[vertices])
  @variable(model, Mu2[vertices])
  @variable(model, Omega[edges])
  @variable(model, Eta)

# Objective function
  @objective(model, Min, sum(Mu1[v] + 2*Mu2[v] for v in vertices)
      + sum(Omega[e] for e in edges) + 2*n*Eta)

# Constraints
  for c in klist
    @constraint(model, 
        sum(Mu1[v] + Mu2[v] for v in c) + 
        sum((Omega[(s,d)] - 1) for s in c for d in c if s<d) +
        Eta >= 0)
  end

  for v in vertices 
    @constraint(model, Mu1[v] <= 0) 
    @constraint(model, Mu2[v] >= 0)
  end

  for e in edges
    @constraint(model, Omega[e] >= 0)
  end

  @constraint(model, Eta >= 0)

  t = time()
  optimize!(model)
  t = time() - t

  return model, Mu1, Mu2, Omega, Eta, t
end

"""
Model of the SUBPROBLEM for Dantzig-Wolfe
used to find the next complete subgraph to add in the list.

Arguments
- vertices : set of all vertices
- edges : set of all edges
- g : graph
- Mu1, Mu2, Omega, Eta : dual variables determined earlier

Return
- model, A, B : model & variables after solve
- t : time of the solve
"""
function subproblem(vertices, edges, g, Mu1, Mu2, Omega, Eta)
  n = length(vertices)
  model = Model(optimizer)
  if @isdefined setup_model
    setup_model(model)
  end

# Variables
  @variable(model, A[vertices], Bin)
  @variable(model, B[edges], Bin)

# Objective function
  @objective(model, Max,
      sum(-A[u]*(value.(Mu1[u]) + value.(Mu2[u])) for u in vertices) +
      sum(B[e]*(1 - value.(Omega[e])) for e in edges) - 
      value.(Eta))

# Constraints - complete subgraphs
  for s in vertices 
    for d in (s+1):n
      if !has_edge(g, s, d) 
        @constraint(model, A[s] + A[d] - 1 <= 0)
      else 
        @constraint(model, A[s] + A[d] - 1 <= B[(s,d)])
        @constraint(model, B[(s,d)] <= A[s])
        @constraint(model, B[(s,d)] <= A[d])
      end 
    end
  end

  t = time()
  optimize!(model)
  t = time() - t

  return model, A, B, t
end

"""
Dantzig Wolfe Column generations method for our problem.
(main)

Argument
- g : graph

Return
- klist : list of generated klicks
- t : time of solve
"""
function dantzig_wolfe(g::SimpleGraph)
  t = time()
  n = nv(g)
  m = ne(g)

  vertices = 1:n

  edges = []
  for s in vertices 
    for d in (s+1):n
      if has_edge(g, s, d)
        push!(edges, (s,d))
      end
    end
  end

# List of complete subgraphs
  klist = [[s, d] for (s, d) in edges]

  val = 1
  
  t = time() - t

  while val > 0
    # Solving dual problem, dp stands for dual pb
    dp, Mu1, Mu2, Omega, Eta, dt = dual(vertices, edges, klist)

    # Solving subproblem, sp stands for sub pb
    sp, A, B, st = subproblem(vertices, edges, g, Mu1, Mu2, Omega, Eta)
    
    t = t + dt + st

    t2 = time()
    val = objective_value(sp)

# We do not need more columns
    if val <= 0 
      break
    end

# Current complete subgraph
    klick = []
    for v in vertices
      if value.(A[v]) == 1 
        push!(klick, v)
      end
    end 

    push!(klist, klick)

    t2 = t2 - time()
    t = t + t2
  end

  return klist, t
end

"""
Run tests for the Dantzig-Wolfe method

Argument
- None

Print
- #id of the test | number of generated columns | total time of the generation
"""
function run_unit_tests()
  path = "unit_tests/TU_0"
  println("#id, nb_cols, time(s)")
  for i in 1:9
    p = string(path, i)

    g = generate(p)

    klist, t = dantzig_wolfe(g)
    println(i, " ", length(klist), " ",  t)
  end
end

if abspath(PROGRAM_FILE) == @__FILE__
  run_unit_tests()
end
