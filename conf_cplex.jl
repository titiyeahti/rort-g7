println("Configure environnement for CPLEX...")
using JuMP
using LightGraphs
using CPLEX

optimizer = CPLEX.Optimizer

function set_verbose(model) 
  set_optimizer_attribute(model, "CPX_PARAM_SCRIND", 1)
end

function setup_model(model)
  set_optimizer_attribute(model,"CPX_PARAM_TILIM",300)
  set_optimizer_attribute(model, "CPX_PARAM_SCRIND", 0)
end

# File used to generate, read and write instances.
include("./instances_io.jl")
println("Done.")
