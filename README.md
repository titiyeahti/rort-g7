# RORT-g7

L'objectif de ce projet était d'étudier le problème de plus proche line-graph.
Dans un premier temps, nous avons travaillé sur deux méthodes de 
résolutions exactes basées sur des programmes linéaires en nombres entiers.
Dans un second temps nous avons travaillé sur la décomposition 
de *Dantzig-Wolfe* du premier modèle.
Ce projet devait être codé en **Julia**.

## Organisation des fichiers
* `LineGraph.pdf`, le sujet du projet.
* Code source des algorithmes :
  - `plne1_v[1-2].jl`, implantation des deux PLNE correspondants décrits dans le sujet.
  - `dantzig_wolfe.jl`, implantation de la décomposition de *Dantzig-Wolfe* (uniquement la génération de colonnes).
* `conf_[solveurs].jl`, fichiers de configurations spécifiques aux solveurs qui doivent être inclus avant les algorithmes.
* `eval_perf/`
  - `eval_perf.jl`, générations des jeux de test, tests et tracé de courbes.
  - `Figures*`, courbes générées par `eval_perf.jl`.
  - `data[A-B]`, jeux de test.
  - `res[A-B]`, résultats des expériences.
* `unit_tests`, jeu de test unitaires fournis avec le projet.
* `instances_io.jl`, utilitaire de création et de lecture d'instances.

## Utilisation 
### Fichiers de configuration 
Pour simplifier l'utilisation de différents solveurs sur le projet, nous 
avons mis en place un système de fichiers de configuration. Pour chaque 
solveur, il faut donc écrire un fichier de configuration de la forme :
```julia
using JuMP
using LightGraphs
using SOLVEUR_CHOISI
include("./instances_io.jl")

optimizer = SOLVEUR_CHOISI.Optimizer

function set_verbose(model)
# configuration du mode verbose pour model
end

# !!OPTIONNEL!!
function setup_model(model)
# configuration générales des options du solveur
end
```

### Utiliser un algorithme sur un graphe g
Pour utiliser un des algorithmes présentés plus haut, il faut commencer par inclure le fichier de configuration pour le solveur voulu (par défaut **GLPK**) puis l'algo voulu:
```julia
julia> include("conf_solveur.jl")
julia> include("algo.jl")
```
Il suffit ensuite d'appeler la fonction de votre choix parmi :
- `plne1_solve(g, verbose(=false))`
- `plne2_solve(g, p::Float64, verbose(=false))`
- `dantzig_wolfe(g)`


Un prototype plus précis de ces fonctions est présent dans le code.

## Remarques et bugs
Selon les solveurs et les algorithmes étudiés, nous avons des résultats 
très différents.

### GLPK
La résolution avec **GLPK** des PLNE1 et PLNE2 est parfois très longue, y 
compris sur des instances de petite taille (exemple : `unit_tests/TU_03` qui 
n'a que 6 sommets).

### CPLEX
Notre implantation de l'algorithme de génération de colonne
de *Dantzig-Wolfe* bloque sur le septième test unitaire lorsque que l'on 
fait tourner l'algorithme en utilisant le solveur **CPLEX**.
