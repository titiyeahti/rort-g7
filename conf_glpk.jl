println("Configure environnement for GLPK...")
using JuMP
using LightGraphs
using GLPK

optimizer = GLPK.Optimizer

function set_verbose(model) 
  set_optimizer_attribute(model, "msg_lev", GLPK.GLP_MSG_ON)
end

# File used to generate, read and write instances.
include("./instances_io.jl")
println("Done.")
